<?php

namespace App\Entity;

use App\Repository\ProjetRepository;
use Cocur\Slugify\Slugify;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: ProjetRepository::class)]
/**
 * @Vich\Uploadable
 */
class Projet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $nom = null;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="projet_image", fileNameProperty="imageNameImg", size="imageSize")
     *
     * @var File|null
     */
    private ?File $image = null;

    #[ORM\Column(type: 'string', nullable:true)]
    private ?string $imageNameImg = null;

    #[ORM\Column(type: 'string', nullable:true)]
    private ?string $imageNameVi = null;

    #[ORM\Column(type: 'integer', nullable:true)]
    private ?int $imageSize = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lienGit = null;

    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\Column(length: 20)]
    private ?string $anneeRealisation = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $lienSite = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $contenu = null;

    #[ORM\Column(length: 50)]
    private ?string $langage = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }



    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $image
     */
    public function setImage(?File $image = null): void
    {
        $this->image = $image;

        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            //$this->updatedAt = new \DateTimeImmutable();
        }
    }
    public function getImage(): ?File
    {
        return $this->image;
    }

    public function getLienGit(): ?string
    {
        return $this->lienGit;
    }

    public function setLienGit(?string $lienGit): self
    {
        $this->lienGit = $lienGit;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAnneeRealisation(): ?string
    {
        return $this->anneeRealisation;
    }

    public function setAnneeRealisation(string $anneeRealisation): self
    {
        $this->anneeRealisation = $anneeRealisation;

        return $this;
    }

    public function getLienSite(): ?string
    {
        return $this->lienSite;
    }

    public function setLienSite(?string $lienSite): self
    {
        $this->lienSite = $lienSite;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function setImageNameImg(?string $imageNameImg): void
    {
        $this->imageNameImg = $imageNameImg;
    }

   public function getImageNameImg(): ?string
    {
        return $this->imageNameImg;
    }


    /**
     * Get the value of slug
     */
    public function getSlug()
    {
        return (new Slugify())->slugify($this->getNom());
    }

    public function getLangage(): ?string
    {
        return $this->langage;
    }

    public function setLangage(string $langage): self
    {
        $this->langage = $langage;

        return $this;
    }

}
