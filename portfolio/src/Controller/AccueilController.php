<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccueilController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('accueil/index.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }


    #[Route('/aboutMe', name: 'about')]
    public function about(): Response
    {
        return $this->render('accueil/about.html.twig', [
            'controller_name' => 'AccueilController',
        ]);
    }
}
