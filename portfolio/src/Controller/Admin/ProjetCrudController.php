<?php

namespace App\Controller\Admin;

use App\Entity\Projet;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProjetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Projet::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('nom', 'Nom du projet'),
            TextEditorField::new('contenu', 'Description du projet'),
            ChoiceField::new('type', 'Type du projet')
                ->autocomplete()
                ->setChoices(['Stage'=>'Stage',
                    'Entreprise'=>'Entreprise',
                    'Formation' => 'Formation',
                    'Personnelle' => 'Personnelle'
                    ]),
            TextField::new('langage', 'Langage utilisé'),
            TextField::new('anneeRealisation', 'Année de la réalisation'),
            TextField::new('imageNameImg', 'Nom Image')->hideOnForm(),
            Field::new('image')->setFormType(VichImageType::class)->hideOnIndex(),
            TextField::new('lienGit'),
            TextField::new('lienSite')

        ];
    }

    public function createEntity(string $entityFqcn)
    {
        //creation d'un projet vide
        $projet = new Projet();
        return $projet;
    }
}
