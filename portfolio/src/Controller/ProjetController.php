<?php

namespace App\Controller;

use App\Entity\Projet;

use App\Repository\ProjetRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjetController extends AbstractController
{

    #[Route('/projet/{id}-{slug}', name: 'show_projet')]
    public function showProjet(ManagerRegistry $doctrine, Request $request, $id, $slug): Response
    {

        $repository = $doctrine->getRepository(Projet::class);
        $projet = $repository->find($id);
        return $this->render('projet/show_projet.html.twig',[
            'projet'=>$projet
        ]);
    }

    #[Route('/projet', name: 'projet')]
    public function showAllProjet(ProjetRepository $projetRepository,  PaginatorInterface $paginato ,Request $request): Response
    {

        $page = $paginato->paginate(
        //recupération du répository de projet
            $projetRepository->findAll(),
            $request->query->getInt('page',1),
            6
        );


        return $this->render('projet/index.html.twig', [
            'page' => $page,
        ]);
    }



}
